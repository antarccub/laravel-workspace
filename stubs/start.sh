#!/bin/bash
echo [$(date)] STARTING UP CONTAINER...

echo [$(date)] $ cd /var/www/html/app/
cd /var/www/html/app/

echo [$(date)] $ composer install
composer install

echo [$(date)] $ chown -R www-data:www-data .
chown -R www-data:www-data .
echo [$(date)] $ chmod -R 775 .
chmod -R 775 .

echo [$(date)] $ STARTING NGINX SERVER...
service nginx restart

echo [$(date)] $ STARTING PHP-FPM...
service php7.2-fpm start


sleep infinity